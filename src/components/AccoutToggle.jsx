import { Box, Typography } from "@mui/material";
import React, { useState } from "react";
import { VscAccount } from "react-icons/vsc";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import LogoutOutlinedIcon from "@mui/icons-material/LogoutOutlined";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import CircularProgress from "@mui/material/CircularProgress";
import ArchiveIcon from "@mui/icons-material/Archive";
import PeopleAltOutlinedIcon from "@mui/icons-material/PeopleAltOutlined";
import { AppContext } from "../new_components/Pages/MyContext";
import { VscDebugBreakpointUnsupported } from "react-icons/vsc";




export default function AccoutToggle() {
  const storeduserData = localStorage.getItem("userName_storage");
  const userData = JSON.parse(storeduserData);

  const navigate = useNavigate();
  
  const handleNavigate_management = () => {
    navigate("/management");
  };
   const handleNavigate_users = () => {
     navigate("/userspage");
   };
  const [loading, setLoading] = useState(false);

  const handleNavigate_logout = () => {
    // Show loader
    setLoading(true);

    // Simulate a delay, you can replace this with your actual logout process
    setTimeout(() => {
      // Hide loader
      setLoading(false);

      // Navigate after the delay
      navigate("/");
    }, 1000); // Adjust the delay time in milliseconds (e.g., 2000 for 2 seconds)
  };
   const handleNavigate_tickets = () => {
     // Show loader
     setLoading(true);

     // Simulate a delay, you can replace this with your actual logout process
     setTimeout(() => {
       // Hide loader
       setLoading(false);

       // Navigate after the delay
       navigate("/tickets");
     }, 1000); // Adjust the delay time in milliseconds (e.g., 2000 for 2 seconds)
   };
  return (
    <>
      {loading && <CircularProgress sx={style} />}
      <Box
        className="flex flex-col absolute "
        style={header}
        sx={{
          backgroundColor: "white",
          zIndex: 1000,
          boxShadow: 6,
        }}
      >
        <div className=" justify-center text-center">
          <Box
            className="flex flex-row justify-center text-center gap-4 p-1"
            style={Acount_Header}
            sx={{
              backgroundColor: "rgb(17 24 39)",
              margin: "10px",
              borderRadius: "8px",
              padding: "10px",
              transition: "background-color 0.4s ease-in, color 0.2s ease-in",
              "&:hover": {
                backgroundColor: "rgb(75 85 99)",
                color: "#000",
              },
            }}
          >
            <VscAccount color="#fff" className="mt-1" size={25} />
            <Typography className="text-gray-100">{userData.role}</Typography>
          </Box>

          <Box
            className="flex flex-col text-gray-600  p-2"
            sx={{
              flex: 1,
              flexDirection: "column",
              gap: "20px",
              justifyContent: "start",
              alignItems: "start",
              marginLeft: "14px",
            }}
          >
            <Box sx={{ flexGrow: 1 }} />
            <Box
              sx={{
                transition: "color 0.4s ease-in",
                "&:hover": {
                  color: "rgb(2 132 199)",
                },
              }}
              className="flex flex-row justify-center text-center gap-2 cursor-pointer"
              onClick={handleNavigate_management}
            >
              <EditOutlinedIcon sx={{ fontSize: 20 }} />
              <Typography sx={{ fontSize: "14px" }}>Management</Typography>
            </Box>

            <Box
              sx={{
                transition: "color 0.4s ease-in",
                "&:hover": {
                  color: "rgb(2 132 199)",
                },
              }}
              className="flex flex-row justify-center text-center gap-4 cursor-pointer"
              onClick={handleNavigate_users}
            >
              <PeopleAltOutlinedIcon sx={{ fontSize: 20 }} />
              <Typography
                sx={{
                  fontSize: "14px",
                  color: "rgb(75 85 99)",
                  transition: "color 0.4s ease-in",
                  "&:hover": {
                    color: "rgb(2 132 199)",
                  },
                  fontWeight: "light",
                }}
              >
                Users
              </Typography>
            </Box>
            <Box
              sx={{
                transition: "color 0.4s ease-in",
                "&:hover": {
                  color: "rgb(2 132 199)",
                },
              }}
              className="flex flex-row justify-center text-center gap-4 cursor-pointer"
              onClick={handleNavigate_logout}
            >
              <LogoutOutlinedIcon sx={{ fontSize: 20 }} />
              <Typography
                sx={{
                  fontSize: "14px",
                  color: "rgb(75 85 99)",
                  transition: "color 0.4s ease-in",
                  "&:hover": {
                    color: "rgb(2 132 199)",
                  },
                }}
              >
                Log Out
              </Typography>
            </Box>
          </Box>
        </div>
      </Box>
    </>
  );
}
const header = {
  borderRadius: "10px",
  width: "180px",
  marginTop: "65px",
  height: "300px",
  marginLeft:"50px"
 
 
};
 const style = {
   position: "absolute",
   top: "70%",
   left: "50%",
   transform: "translate(-50%, -50%)",
   color: "#1e90ff",
   //  bgcolor: "background.paper",
   //  boxShadow: 6,
   //  p: 4,
   //  borderRadius: 2,
   // overflowY: "auto",
   // height: "80vh",
 };
const Acount_Header = {};
