import { useState } from "react";
import { FaSearch } from "react-icons/fa";

import "./SearchBar.css";
import { Search } from "@mui/icons-material";
import { Box, Button, InputBase, styled } from "@mui/material";


export const SearchBar = ({ setResults }) => {
  const [input, setInput] = useState("");

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const fetchData = (value) => {
    fetch("https://nurse-backend.onrender.com/get-nurse")
      .then((response) => response.json())
      .then((json) => {
        const results = json.filter((user) => {
          return (
            value &&
            user &&
            user.name &&
            user.name.toLowerCase().includes(value)
          );
        });
        setResults(results);
      });
  };

  const handleChange = (value) => {
    setInput(value);
    fetchData(value);
  };

  return (
    // <Box className="flex flex-row bg-gray-100 w-[550px] m-4 p-1 rounded-lg shadow-xl gap-[300px]">
    //   <Search>
    //     <SearchIconWrapper>
    //       <SearchIcon />
    //     </SearchIconWrapper>
    //     <StyledInputBase
    //       //onClick={handleSearchModal}
    //       placeholder="Search…"
    //       inputProps={{ "aria-label": "search" }}
    //     />
    //   </Search>

    //   {/* <Button variant="contained">Search</Button> */}
    // </Box>
    <div className="input-wrapper">
      <FaSearch id="search-icon" />
      <input
        placeholder="Type to search..."
        value={input}
        onChange={(e) => handleChange(e.target.value)}
      />
    </div>
  );
};
