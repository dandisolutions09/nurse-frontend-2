import { useCountUp } from "use-count-up";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import NavigationBar from "../NavigationBar";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TestingTable from "../TestingTable";
import * as Yup from "yup";
import {
  Modal,
  TextField,
  Box,
  MenuItem,
  IconButton,
  InputAdornment,
  LinearProgress,
} from "@mui/material";
import { useFormik } from "formik";
import { Line, Circle } from "rc-progress";
import CloseIcon from "@mui/icons-material/Close";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import LightTooltip from "../../components/LightTool";
import { AppContext } from "./MyContext";

const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  password: Yup.string().required("password is required"),
  role: Yup.string().required("role is required"),
});

const ApiList = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [buttonLabel, setButtonLabel] = React.useState("Start");
  const [percentage, setPercentage] = React.useState();
  const [user_percentage, setPercentageUser] = React.useState();
  const [numberNurses, set_numberNurses] = React.useState();
  const [numberUsers, set_numberUsers] = React.useState();
  const [loading, setLoading] = useState(false);

  const { value: value1, reset: resetValue1 } = useCountUp({
    isCounting: isLoading,
    duration: 1,
    start: 0,
    end: 25,
    onComplete: () => {
      setIsLoading(false);
      setButtonLabel("Reset");
    },
  });

  const { value: value2 } = useCountUp({
    isCounting: true,
    duration: 1,
    start: 0,
    end: 75,
  });

  // const percentage = Math.round((value1 + value2) / 100);

  const handleButtonClick = () => {
    if (isLoading) {
      setIsLoading(false);
      setButtonLabel("Start");
      resetValue1();
    } else if (buttonLabel === "Reset") {
      setButtonLabel("Start");
      resetValue1();
    } else {
      setIsLoading(true);
      setButtonLabel("Reset");
    }
  };

  const fetchDataNurses = async () => {
    try {
      const response = await axios.get(
        "https://nurse-backend.onrender.com/get-nurses"
      );
      const nurseData = response.data.length;
      set_numberNurses(response.data.length);

      console.log("nurseData", nurseData);
      const per = (nurseData / 1000) * 100;
      console.log("percentage", per);
      setPercentage(per.toFixed(2));
    } catch (error) {
      console.error("Error fetching data:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  const fetchDataUsers = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "https://nurse-backend.onrender.com/get-users"
      );
      const userData = response.data.length;
      set_numberUsers(response.data.length);
      //const[numberUsers, set_numberUsers] = React.useState()

      console.log("userdata", userData);
      // per.toFixed(2)
      const per = (userData / 1000) * 100;
      console.log("percentage:->", per);
      setPercentageUser(per.toFixed(2));
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchDataNurses();
    fetchDataUsers();
  }, []);

  return (
    <>
      <NavigationBar />
      <div>
        {loading && <LinearProgress />}

        {/* Your existing component content */}
      </div>
      <Typography
        sx={{
          fontSize: "26px",
          margin: "20px",

          color: "rgb(31 41 55)",
        }}
      >
        Analytics
      </Typography>
  
      <div className="flex flex-col " style={header}>
        <Box sx={{ flexGrow: 1 }} />
        <Box sx={{ flexGrow: 1 }} />
        <div className="flex flex-row gap-3 justify-center">
          {/* Wards Card */}
          <Card
            sx={{
              width: 445,
              boxShadow: 6,
            }}
          >
            {/* <CardMedia height="540" /> */}
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Active Nurses
              </Typography>
        
                <div>
                  <Circle
                    percent={percentage}
                    strokeWidth={9}
                    strokeColor="#333333"
                    trailWidth={9}
                  />
                </div>
            
            </CardContent>
            <CardActions>
              <div className="flex flex-row gap-6">
                <div className="flex flex-row gap-6">
                  Percentage of Nurses:
                  {typeof percentage === "undefined" ? (
                    <Typography gutterBottom variant="h5" component="div">
                      Loading...
                    </Typography>
                  ) : (
                    <Typography gutterBottom variant="h5" component="div">
                      {`${percentage}%`}
                    </Typography>
                  )}
                  {/* Your other component content */}
                </div>
              </div>
            </CardActions>
          </Card>

          {/* Positions Card */}
          <Card
            sx={{
              width: 445,

              boxShadow: 6,
            }}
          >
         
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Inactiver Nurses
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <Circle
                    percent={user_percentage}
                    strokeWidth={9}
                    strokeColor="#ff2400"
                    trailWidth={9}
                  />
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              {/* <div className="flex flex-row gap-6">
                Percentage of Users:
                <Typography
                  gutterBottom
                  variant="h4"
                  component="div"
                >{`${user_percentage}%`}</Typography>
              </div> */}

              <div className="flex flex-row gap-6">
                Percentage of Nurses:
                {typeof user_percentage === "undefined" ? (
                  <Typography gutterBottom variant="h5" component="div">
                    Loading...
                  </Typography>
                ) : (
                  <Typography gutterBottom variant="h5" component="div">
                    {`${user_percentage}%`}
                  </Typography>
                )}
                {/* Your other component content */}
              </div>
            </CardActions>
          </Card>

          {/* Departments Card */}

          <Card
            sx={{
              width: 445,

              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Active Users
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <Circle
                    percent={user_percentage}
                    strokeWidth={9}
                    strokeColor="#333333"
                    trailWidth={9}
                  />
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              {/* <div className="flex flex-row gap-6">
                Percentage of Users:
                <Typography
                  gutterBottom
                  variant="h4"
                  component="div"
                >{`${user_percentage}%`}</Typography>
              </div> */}

              <div className="flex flex-row gap-6">
                Percentage of Nurses:
                {typeof user_percentage === "undefined" ? (
                  <Typography gutterBottom variant="h5" component="div">
                    Loading...
                  </Typography>
                ) : (
                  <Typography gutterBottom variant="h5" component="div">
                    {`${user_percentage}%`}
                  </Typography>
                )}
                {/* Your other component content */}
              </div>
            </CardActions>
          </Card>

          <Card
            sx={{
              width: 445,

              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Inactive Users
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <Circle
                    percent={user_percentage}
                    strokeWidth={9}
                    strokeColor="#333333"
                    trailWidth={9}
                  />
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              {/* <div className="flex flex-row gap-6">
                Percentage of Users:
                <Typography
                  gutterBottom
                  variant="h4"
                  component="div"
                >{`${user_percentage}%`}</Typography>
              </div> */}

              <div className="flex flex-row gap-6">
                Percentage of Nurses:
                {typeof user_percentage === "undefined" ? (
                  <Typography gutterBottom variant="h5" component="div">
                    Loading...
                  </Typography>
                ) : (
                  <Typography gutterBottom variant="h5" component="div">
                    {`${user_percentage}%`}
                  </Typography>
                )}
                {/* Your other component content */}
              </div>
            </CardActions>
          </Card>
          <Card
            sx={{
              width: 445,
              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Data Representation
              </Typography>
               <div>
                  <ul>
                    <li
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>
                      Number of Nurses
                      <Button
                        size="small"
                        // onClick={() => handleRemove("Departments", index)}
                      >
                        {numberNurses}/1000
                      </Button>
                    </li>
                    <li
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>
                      Number of Users
                      <Button
                        size="small"
                        // onClick={() => handleRemove("Departments", index)}
                      >
                        {numberUsers}/1000
                      </Button>
                    </li>
                    <li
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>
                      Number of Users
                      <Button
                        size="small"
                        // onClick={() => handleRemove("Departments", index)}
                      >
                        {numberUsers}/1000
                      </Button>
                    </li>
                    <li
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>
                      Number of Users
                      <Button
                        size="small"
                        // onClick={() => handleRemove("Departments", index)}
                      >
                        {numberUsers}/1000
                      </Button>
                    </li>
                  </ul>
                </div>
          
            </CardContent>
            <CardActions>
              <div className="flex flex-row gap-6 ml-6">
                <Button size="small">More Info...</Button>
              </div>
            </CardActions>
          </Card>
        </div>

        <div className="m-4"></div>
      </div>
    </>
  );
};
const header = {
  border: "1px solid",
  borderColor: "#000",
  margin: "50px",
  padding: "50px",
  borderRadius: "8px",
};

export default ApiList;
