import React, { useEffect, useState } from "react";
import axios from "axios";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import {
  AppBar,
  Box,
  Button,
  LinearProgress,
  Modal,
  Toolbar,
  Typography,
} from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import IconButton from "@mui/material/IconButton";
import NavigationBar from "../NavigationBar";
import { Dropdown } from "antd";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import AccountCircle from "@mui/icons-material/AccountCircle";
import { AppContext } from "./MyContext";
import NurseActivatesuccess from "../../components/NursesAlerts/NurseActivatesuccess";
import NurseDeactivate from "../../components/NursesAlerts/NurseDeactivate";

export default function Nurses() {
  const [data, setData] = useState(null);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = useState(false);
 const [openNurseActivatesuccess, setOpenNurseActivatesuccess] =
   React.useState(false);
    const [openNurseDectivatesuccess, setOpenNurseDectivatesuccess] =
      React.useState(false);
  const { userData } = React.useContext(AppContext);

  //  console.log("context user name:->", userData.username);
  //  console.log("context user name:->", userData.username);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  // const items = [
  //   // {
  //   //   key: "1",
  //   //   label: "View Nuser",
  //   //   icon: <AccountCircle sx={{ fontSize: 32 }} />,
  //   //   // onClick: () => handleOpenViewUser(selectedRowData),
  //   // },

  //   {
  //     key: "1",
  //     label: (
  //       <span style={{ color: "red", fontWeight: "bold" }}>Activate Nurse</span>
  //     ),
  //     // icon: <DeleteForeverIcon />,
  //     onClick: () => handleOPen_confirm(selectedRowData),
  //   },
  //   // {
  //   //   key: "3",
  //   //   label: "Reset User Password",
  //   //   icon: <DeleteForeverIcon />,
  //   //   // onClick: () => handleOpenEdit(selectedRowData),
  //   // },
  // ];
  const columns = [
    { id: "name", label: "NURSE NAME", minWidth: 100 },
    { id: "department", label: "DEPT", minWidth: 100 },
    { id: "position", label: "POSITION", minWidth: 100 },
    { id: "wards", label: "WARDS", minWidth: 100 },
    { id: "duration_days", label: "DURATION(days)", minWidth: 100 },
    // { id: "duration", label: "Duration(hrs)", minWidth: 100 },
    { id: "status", label: "STATUS", minWidth: 100 },
    // { id: "color", label: "color", minWidth: 100 },
    { id: "created_at", label: "TIMESTAMP", minWidth: 100 },
  ];

  // const fetchData = async () => {

  //   setLoading(true)
  //   try {
  //     const response = await axios.get(
  //       "https://nurse-backend.onrender.com/get-nurses"
  //     );
  //     const json = response.data;

  //     const now = new Date();

  //     json.forEach((item) => {
  //       const durationMs = now - new Date(item.created_at);
  //       const durationMinutes = durationMs / (1000 * 60);
  //       const durationDays = durationMinutes / (24 * 60);
  //       const durationHours = durationMs / (1000 * 60 * 60);

  //       item.duration = durationHours.toFixed(0);
  //       item.duration_days = durationDays.toFixed(0);
  //       //item.color = durationDays > 60 ? "red" : "green";

  //       const differenceTo60 = Math.abs(durationDays - 60);

  //       if (durationDays > 60) {
  //         item.color = "red";

  //         //update nurse information to inactive
  //       } else if (differenceTo60 < 10) {
  //         item.color = "yellow"; // Amber
  //       } else {
  //         item.color = "#74B72E";
  //       }
  //     });

  //     if (json && Array.isArray(json) && json.length > 0) {
  //       json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
  //       setRows(json);
  //     } else {
  //       console.warn("Received null, undefined, or empty response.");
  //     }
  //   } catch (error) {
  //     console.error("Error fetching data:", error);
  //   }
  // };

  const fetchData = async () => {
    setLoading(true); // Set loading to true when making the request

    try {
      const response = await axios.get(
        "https://nurse-backend.onrender.com/get-nurses"
      );
      const json = response.data;

      const now = new Date();

      json.forEach((item) => {
        const durationMs = now - new Date(item.created_at);
        const durationMinutes = durationMs / (1000 * 60);
        const durationDays = durationMinutes / (24 * 60);
        const durationHours = durationMs / (1000 * 60 * 60);

        item.duration = durationHours.toFixed(0);
        item.duration_days = durationDays.toFixed(0);

        const differenceTo60 = Math.abs(durationDays - 60);

        if (durationDays > 60) {
          item.color = "red";
        } else if (differenceTo60 < 10) {
          item.color = "yellow";
        } else {
          item.color = "#74B72E";
        }
      });

      if (json && Array.isArray(json) && json.length > 0) {
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        setRows(json);
      } else {
        console.warn("Received null, undefined, or empty response.");
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };
  const [selectedRowData, setSelectedRowData] = React.useState("");
  const [openConfirm, setOpenConfirm] = useState(false);
  const [activating_modal, setActivating_modal] = useState(false);
  const [deactivating_modal, setDeactivating_modal] = useState(false);

  useEffect(() => {
    fetchData();
  }, [deactivating_modal, activating_modal]);

  const handleOPen_confirm = (nursedata) => {
    if (nursedata.status == "ACTIVE") {
      console.log("nurse already active");
      setDeactivating_modal(true);
    } else if (nursedata.status == "INACTIVE") {
      console.log("nurse already inactive");
      setActivating_modal(true);
    }

    // setOpenActivating_modal(true);
    // setOpenDeactivating_modal(true);
  };
  const handleClose_active = () => {
    setActivating_modal(false);
  };
  const handleClose_deactive = () => {
    setDeactivating_modal(false);
  };

  function deactivateNurse(id) {
    console.log("deactivated id", id);
    // Replace with your server's endpoint
    const apiUrl = `https://nurse-backend.onrender.com/deactivate-nurse/${id}`;

    // Make a POST request to the server
    return fetch(apiUrl, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle the response from the server
        console.log(data);
        setOpenNurseDectivatesuccess(true);
         handleClose_deactive();
      })
      .catch((error) => {
        console.error("Error:", error);
      });
     
  }


    function activateNurse(id) {
      console.log("activate id", id);
      // Replace with your server's endpoint
      const apiUrl = `https://nurse-backend.onrender.com/activate-nurse/${id}`;

      // Make a POST request to the server
      return fetch(apiUrl, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((data) => {
          // Handle the response from the server
          console.log(data);
          setOpenNurseActivatesuccess(true);
          handleClose_active();
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }

  return (
    <>
      <NurseDeactivate
        open={openNurseDectivatesuccess}
        autoHideDuration={3000}
        onClose={() => setOpenNurseDectivatesuccess(false)}
      />
      <NurseActivatesuccess
        open={openNurseActivatesuccess}
        autoHideDuration={3000}
        onClose={() => setOpenNurseActivatesuccess(false)}
      />
      <Modal
        open={activating_modal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography className="flex flex-row gap-2">
            A you sure you want to{" "}
            <span className="text-green-700 font-bold">Activate</span>
            <Typography sx={{ fontWeight: "bold" }}>
              {selectedRowData.name}
            </Typography>
            ?
          </Typography>
          <div className="flex flex-row gap-4 m-4 justify-center mt-8">
            <Button
              variant="contained"
              color="success"
              onClick={() => activateNurse(selectedRowData._id)}
            >
              Activate
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={handleClose_active}
            >
              Cancel
            </Button>
          </div>
        </Box>
      </Modal>
      <Modal
        open={deactivating_modal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography className="flex flex-row gap-2">
            A you sure you want to
            <span className="text-red-600 font-bold"> Deactivate </span>
            <Typography sx={{ fontWeight: "bold" }}>
              {selectedRowData.name}
            </Typography>
            ?
          </Typography>
          <div className="flex flex-row gap-4 m-4 justify-center mt-8">
            <Button
              variant="contained"
              onClick={() => deactivateNurse(selectedRowData._id)}
            >
              Deactivate
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={handleClose_deactive}
            >
              Cancel
            </Button>
          </div>
        </Box>
      </Modal>
      <NavigationBar />
      <div>
        {loading && <LinearProgress />}

        {/* Your existing component content */}
      </div>
      <Typography
        sx={{
          fontSize: "26px",
          margin: "20px",

          color: "rgb(31 41 55)",
        }}
      >
        Nurses
      </Typography>
      <Paper
        sx={{
          width: "100%",
          overflow: "hidden",
          padding: "30px",
          margin: "20px",
        }}
      >
        <TableContainer sx={{ maxHeight: "100%" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    sx={{ fontWeight: "600", fontSize: "16px" }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    // <Dropdown
                    //   key={row._id}
                    //   menu={{ items }}
                    //   trigger={["contextMenu"]}
                    //   placement="bottomLeft"
                    // >
                    <TableRow
                      role="checkbox"
                      tabIndex={-1}
                      key={row._id}
                      sx={{
                        transition: "background-color 0.4s ease",
                        "&:hover": { backgroundColor: "rgb(101 163 13)" },
                        cursor: "pointer",
                        backgroundColor: row.color,
                        select: "none",
                      }}
                      onMouseEnter={() => {
                        setSelectedRowData(row);
                      }}
                      onDoubleClick={() => handleOPen_confirm(row)}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                    // </Dropdown>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 8,
  borderRadius: 2,
};
