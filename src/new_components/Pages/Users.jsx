import { Box, Button, LinearProgress, Typography } from "@mui/material";
import React from "react";
import LightTooltip from "../../components/LightTool";
import NavigationBar from "../NavigationBar";
import TestingTable from "../TestingTable";
import {
  Modal,
  TextField,
  MenuItem,
  IconButton,
  InputAdornment,
} from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";
import CloseIcon from "@mui/icons-material/Close";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import axios from "axios";
import { AppContext } from "./MyContext";
import SuccessSnackbar from "../SuccessSnackbar";

const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  password: Yup.string().required("password is required"),
  role: Yup.string().required("role is required"),
});

export default function Users() {
  const { userData } = React.useContext(AppContext);
  const [showPassword, setShowPassword] = React.useState(false);
  const [openAddUser, setOpenAddUser] = React.useState(false);
  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
    // overflowY: "auto",
    // height: "80vh",
  };
  const role = [
    {
      value: "Admin",
      label: "Admin",
    },
    {
      value: "Intermediate",
      label: "Intermediate",
    },
    {
      value: "Basic",
      label: "Basic",
    },
  ];
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleOpenAddUser = () => {
    setOpenAddUser(true);
  };
  const handleCloseAddUser = () => {
    setOpenAddUser(false);
  };
  const postData = async (data) => {
    setLoading(true)
    console.log("user to be registered", data);

    const user_json = {
      username: data.name,
      password: data.password,
      status: "Active",
      role: data.role,
    };

    console.log("user json", user_json);
    try {
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        // "https://nurse-backend.onrender.com/add-nurse",
        "https://nurse-backend.onrender.com/register",
        user_json
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
      //  setOpenSuccessAlert(true);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
    // alert("nurse saved successfully...");

    handleCloseAddUser();
    setOpenSuccessAlert(true);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
      role: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data:-", values);

      postData(values);
    },
  });
  return (
    <>
      <SuccessSnackbar
        open={openSuccessAlert}
        autoHideDuration={3000}
        onClose={() => setOpenSuccessAlert(false)}
      />

      <Modal
        open={openAddUser}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add New User
            </Typography>

            <Button
              onClick={handleCloseAddUser}
              color="error"
              variant="contained"
            >
              <CloseIcon fontSize="40" />
            </Button>
          </div>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  label="name"
                  helperText="Please enter full name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                />

                <TextField
                  id="role"
                  select
                  label="Role"
                  name="role"
                  value={formik.values.role}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  helperText="Please select role"
                  error={formik.touched.role && Boolean(formik.errors.role)}
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  type={showPassword ? "text" : "password"}
                  id="password"
                  name="password"
                  label="password:"
                  placeholder="password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  maxRows={4}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                />
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button type="submit" variant="contained" color="success">
                Save
              </Button>
              <Button
                onClick={handleCloseAddUser}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <NavigationBar userName={userData.username} />
      <div>
        {loading && <LinearProgress />}

        {/* Your existing component content */}
      </div>

      <Box sx={{ flexGrow: 1 }} />
      <Box
        component="section"
        sx={{
          p: 2,
          boxShadow: 10,

          margin: "4px",
          borderRadius: "8px",
        }}
      >
        <Box className="flex justify-between">
          <Typography variant="h5">Users</Typography>
          <LightTooltip title="Add New User">
            <Button variant="contained" onClick={handleOpenAddUser}>
              Add +
            </Button>
          </LightTooltip>
        </Box>
        <TestingTable />
      </Box>
    </>
  );
}
